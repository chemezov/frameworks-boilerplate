from flask import request, jsonify, url_for
from app.models import CallCenter
from app.api import bp
from app import db
from app.api.errors import bad_request


@bp.route('/callcenter/<int:id>', methods=['GET'])
def get_item(id):
    return jsonify(CallCenter.query.get_or_404(id).to_dict())


@bp.route('/callcenter', methods=['GET'])
def get_items():
    page = request.args.get('page', 1, type=int)

    per_page = min(request.args.get('per_page', 10), 100)

    data = CallCenter.to_collection_dict(CallCenter.query, page, per_page, 'api.get_items')
    
    return jsonify(data)


@bp.route('/callcenter', methods=['POST'])
def create_item():

    data = request.get_json() or {}

    if not data:
        return bad_request('No data found')

    item = CallCenter()

    item.from_dict(data)

    db.session.add(item)

    db.session.commit()


    response = jsonify(item.to_dict())

    response.status_code = 201

    response.headers['Location'] = url_for('api.get_item', id=item.id)
    return response


@bp.route('/callcenter/<int:id>', methods=['PUT'])
def update_item(id):
    item = CallCenter.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('No data found')
    item.from_dict(data)
    db.session.commit()
    return jsonify(item.to_dict())

@bp.route('/callcenter/<int:id>', methods=['DELETE'])
def remove_item(id):
    item = CallCenter.query.get_or_404(id)
    db.session.delete(item)
    db.session.commit()
    return jsonify(item.to_dict())
